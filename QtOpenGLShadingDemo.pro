#-------------------------------------------------
#
# Project created by QtCreator 2014-09-03T09:29:30
#
#-------------------------------------------------

QT       += core gui opengl xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtOpenGLShadingDemo
TEMPLATE = app


SOURCES += code/main.cpp\
           code/mainwindow.cpp \
           code/trackball.cpp \
           code/mesh.cpp \
           code/lighting.cpp \
           code/openglwidget.cpp \
           code/openglbackground.cpp \
           code/configuration.cpp

HEADERS += code/mainwindow.h \
           code/trackball.h \
           code/mesh.h \
           code/lighting.h \
           code/openglwidget.h \
           code/openglbackground.h \
           code/configuration.h

FORMS   += code/mainwindow.ui

RESOURCES += resources.qrc

LIBS += $$PWD/bin/libassimp.dll

INCLUDEPATH += include
