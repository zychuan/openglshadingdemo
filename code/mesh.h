/**************************************************
 *
 * 機能： モデルをロード、描画する
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#ifndef MESH_H
#define MESH_H

#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/cimport.h>
#include <QMap>
#include <QDir>
#include <QFileInfo>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>

class Mesh
{
public:
    Mesh(QOpenGLShaderProgram *program);
    ~Mesh();

    /*
     * 機能： 指定だれたファイルパスのモデルファイルをロードし、解析する
     */
    bool loadMesh(QString filename);

    /*
     * 機能： モデルを描画する
     */
    void draw();

    /*
     * 機能： 拡大縮小倍率を設定する
     */
    void setModelScale(float ratio);

    /*
     * 機能： 計算したモデル拡大縮小倍率を取得する
     */
    float getModelScaleRatio();

    /*
     * 機能： モデルの最大最少又は中心座標
     */
    QVector3D min, max, center;

private:

    /*
     * 機能： ロードしたテクスチャをクリアする
     */
    void clear();

    /*
     * 機能： Assimpがロードしたモデルから、頂点法線などの情報を取得し、メッシュオブジェクトを作成する
     */
    bool initFromScene(const aiScene* pScene, QDir baseDir);

    /*
     * 機能： モデルの指定されたノードのバウンディングボックスの最大最少座標を求める
     */
    void getBoundingBoxForNode (aiNode* nd,
                                aiVector3D* min,
                                aiVector3D* max,
                                aiMatrix4x4* trafo
                                );

    /*
     * 機能： モデルのバウンディングボックスの最大最少座標を求める
     */
    void getBoundingBox (aiVector3D* min, aiVector3D* max);

    // aiColor4D型をfloat配列に変換する
    void color4ToFloat4(aiColor4D *c, float f[4]);
    // float配列に値を設定する
    void setFloat4(float f[4], float a, float b, float c, float d);

    /*
     * 機能： モデルの指定されたノードを描画する
     */
    void renderNode(aiNode* nd, aiMatrix4x4* trafo);

    // モデルのメッシュ構造体
    struct MeshEntry {
        MeshEntry();

        ~MeshEntry();

        // VBO
        QOpenGLBuffer *m_vertexBuffer;
        QOpenGLBuffer *m_indexBuffer;
        QOpenGLBuffer *m_normalBuffer;
        QOpenGLBuffer *m_textureBuffer;

        unsigned int indicesNum;         // メッシュの頂点数
        QString textureName;             // メッシュが対応するテクスチャファイルのパス

        // メッシュの材質
        struct {
            QVector4D diffuse;
            QVector4D ambient;
            QVector4D specular;
            QVector4D emission;
            float shininess;
            int texCount;
        } m_material;
    };

    // Assimp がロードしたモデルオブジェクト
    const aiScene* scene;

    // 描画用のシェーダプログラム
    QOpenGLShaderProgram *m_program;


    int VERTEX_POS_ID;
    int VERTEX_NORM_ID;
    int TEXTURE_COORD_ID;
    // シェーダ内のにノード変換行列"NodeTrafo"へのハンドルID
    int NODE_TRANS_MATRIX_ID;

    // シェーダ内のに材質構造体"gMaterial"へのハンドルID
    struct {
        int diffuse;
        int ambient;
        int specular;
        int emission;
        int shininess;
        int texCount;
    }MATERIAL_ID;

    // 設定の拡大縮小倍率
    float magnification;
    // モデルの最大の長さ
    float modelLength;
    // 計算したモデル拡大縮小倍率
    float scaleRatio;

    // すべてのメッシュを保存する
    QVector<MeshEntry> m_entries;

    // すべてのテクスチャオブジェクトを保存する(key: テクスチャファイルのパス)
    QMap<QString, QOpenGLTexture*> m_textures;

};

#endif // MESH_H
