#include "TrackBall.h"
#include <QLineF>
#include <QtMath>

TrackBall::TrackBall(TrackMode mode)
    : m_angularVelocity(0)
    , m_paused(true)
    , m_pressed(false)
    , m_mode(mode)
{
    m_axis = QVector3D(0, 1, 0);
    m_rotation = QQuaternion();
    m_lastTime = QTime::currentTime();
}

TrackBall::TrackBall(float angularVelocity, const QVector3D& axis, TrackMode mode)
    : m_axis(axis)
    , m_angularVelocity(angularVelocity)
    , m_paused(true)
    , m_pressed(false)
    , m_mode(mode)
{
    m_rotation = QQuaternion();
    m_lastTime = QTime::currentTime();
}

TrackBall::TrackBall(float angularVelocity, const QVector3D& axis, const QQuaternion& rotation, TrackMode mode)
    : m_rotation(rotation)
    , m_axis(axis)
    , m_angularVelocity(angularVelocity)
    , m_paused(true)
    , m_pressed(false)
    , m_mode(mode)
{
    m_lastTime = QTime::currentTime();
}

void TrackBall::push(const QPointF& p, const QQuaternion &)
{
    m_rotation = rotation();
    m_pressed = true;
    m_lastTime = QTime::currentTime();
    m_lastPos = p;
    m_angularVelocity = 0.0f;
}

void TrackBall::move(const QPointF& p, const QQuaternion &transformation, bool outRotate)
{
    if (!m_pressed)
        return;

    QTime currentTime = QTime::currentTime();
    int msecs = m_lastTime.msecsTo(currentTime);
    //    if (msecs <= 20)
    //        return;

    switch (m_mode) {
    case Plane:
    {
        QLineF delta(m_lastPos, p);
        m_angularVelocity = 180*delta.length() / (PI*msecs);
        m_axis = QVector3D(-delta.dy(), delta.dx(), 0.0f).normalized();
        m_axis = transformation.rotatedVector(m_axis);
        m_rotation = QQuaternion::fromAxisAndAngle(m_axis, 180 / PI * delta.length()) * m_rotation;
    }
        break;
    case Sphere:
    {
        QVector3D lastPos3D;
        float y = m_lastPos.y() * 1920 / 1080;
        if(outRotate)
            lastPos3D = QVector3D(m_lastPos.x(), y, 0.0f);
        else
            lastPos3D = QVector3D(-m_lastPos.x(), -y, 0.0f);

        float sqrZ = 1 - QVector3D::dotProduct(lastPos3D, lastPos3D);
        if (sqrZ > 0)
            lastPos3D.setZ(qSqrt(sqrZ));
        else
            lastPos3D.normalize();

        QVector3D currentPos3D;
        y = p.y() * 1920 / 1080;
        if(outRotate)
            currentPos3D = QVector3D(p.x(), y, 0.0f);
        else
            currentPos3D = QVector3D(-p.x(), -y, 0.0f);
        sqrZ = 1 - QVector3D::dotProduct(currentPos3D, currentPos3D);
        if (sqrZ > 0)
            currentPos3D.setZ(qSqrt(sqrZ));
        else
            currentPos3D.normalize();

        m_axis = QVector3D::crossProduct(lastPos3D, currentPos3D);
        float angle = 180 / PI * asin(qSqrt(QVector3D::dotProduct(m_axis, m_axis)));

        m_angularVelocity = angle / msecs;
        m_axis.normalize();
        m_axis = transformation.rotatedVector(m_axis);
        m_rotation = QQuaternion::fromAxisAndAngle(m_axis, angle) * m_rotation;
    }
        break;
    }


    m_lastPos = p;
    m_lastTime = currentTime;
}

void TrackBall::release(const QPointF& p, const QQuaternion &transformation)
{
    move(p, transformation);
    m_pressed = false;
}

void TrackBall::start()
{
    m_lastTime = QTime::currentTime();
    m_paused = false;
}

void TrackBall::stop()
{
    m_rotation = rotation();
    m_paused = true;
}

QQuaternion TrackBall::rotation() const
{
    if (m_paused || m_pressed)
        return m_rotation;

    QTime currentTime = QTime::currentTime();
    float angle = m_angularVelocity * m_lastTime.msecsTo(currentTime);
    return QQuaternion::fromAxisAndAngle(m_axis, angle) * m_rotation;
}
