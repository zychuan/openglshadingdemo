/**************************************************
 *
 * 機能： 背景の描画
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-10-06
 *
 **************************************************/

#include "openglbackground.h"

OpenGLBackGround::OpenGLBackGround(QOpenGLShaderProgram *program)
    : m_program(program)
    , m_bg(0)
    , m_topText(0)
    , m_light(0)
    , opacityText(1.0)
    , opacityLight(1.0)
    , o_timer(new QTimer())
{
    S_MVP_ID = program->uniformLocation("MVP");
    S_OPACITY_TEXT_ID = program->uniformLocation("opacityText");
    S_OPACITY_LIGHT_ID = program->uniformLocation("opacityLight");

    VERTEX_POS_ID = m_program->attributeLocation("vertexPos_m");
    TEXTURE_COORD_ID = m_program->attributeLocation("texture_coord");

    connect(o_timer, SIGNAL(timeout()), SLOT(fadeInStep()));
}

OpenGLBackGround::~OpenGLBackGround()
{
    m_program = NULL;
    if(0 != m_bg)
        m_bg->destroy();
    if(0 != m_topText)
        m_topText->destroy();
    if(0 != m_light)
        m_light->destroy();
    delete o_timer;
}

void OpenGLBackGround::loadBackGround(QString bg,
                                      QString light,
                                      QString topText)
{
    QImage img(bg);
    m_bg = new QOpenGLTexture(img);
    m_light = new QOpenGLTexture(QImage(light));
    m_topText = new QOpenGLTexture(QImage(topText));

    QVector3D vertices[20] =
    {
        QVector3D(1.0f, -1.0f, 1.0f),
        QVector3D(1.0f, 1.0f, 1.0f),
        QVector3D(-1.0f, -1.0f, 1.0f),
        QVector3D(-1.0f, 1.0f, 1.0f),

        QVector3D(0.8222222222222222f, 0.1927083333333333f, 0.3f),
        QVector3D(0.8222222222222222f, -0.9552083333333333f, 0.3f),
        QVector3D(-1.177777777777778f, 0.1927083333333333f, 0.3f),
        QVector3D(-1.177777777777778f, -0.9552083333333333f, 0.3f),


        QVector3D(0.5092592592592593f, 0.4197916666666667f, 0.4f),
        QVector3D(0.5092592592592593f, 0.56875f, 0.4f),
        QVector3D(-0.5203703703703704f, 0.4197916666666667f, 0.4f),
        QVector3D(-0.5203703703703704f, 0.56875f, 0.4f),
    };

    m_vertexBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_vertexBuffer->create();
    m_vertexBuffer->setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_vertexBuffer->bind();
    m_vertexBuffer->allocate(&vertices[0], sizeof(QVector3D)*3*20);

    QVector2D vTexCoords[20] = {
        QVector2D(0.0f, 1.0f),
        QVector2D(0.0f, 0.0f),
        QVector2D(1.0f, 1.0f),
        QVector2D(1.0f, 0.0f),

        QVector2D(0.0f, 1.0f),
        QVector2D(0.0f, 0.0f),
        QVector2D(1.0f, 1.0f),
        QVector2D(1.0f, 0.0f),

        QVector2D(0.0f, 1.0f),
        QVector2D(0.0f, 0.0f),
        QVector2D(1.0f, 1.0f),
        QVector2D(1.0f, 0.0f),
    };

    m_textureBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_textureBuffer->create();
    m_textureBuffer->setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_textureBuffer->bind();
    m_textureBuffer->allocate(&vTexCoords[0], sizeof(QVector2D)*2*20);


    QMatrix4x4 s_model;
    QMatrix4x4 s_ortho;
    QMatrix4x4 s_view;
    s_ortho.ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 1000.0f);
    s_view.lookAt(QVector3D(0.0f, 0.0f, 0.0f), QVector3D(0.0f, 0.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f));
    s_mvp = s_ortho * s_view * s_model;
}

void OpenGLBackGround::draw()
{
    //Transparent
    glEnable(GL_BLEND);
    glEnable(GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    m_program->bind();
    m_program->setUniformValue(S_MVP_ID, s_mvp);
    m_program->setUniformValue(S_OPACITY_TEXT_ID, opacityText);
    m_program->setUniformValue(S_OPACITY_LIGHT_ID, opacityLight);

    glDepthMask(0);
    m_program->enableAttributeArray(VERTEX_POS_ID);
    m_program->enableAttributeArray(TEXTURE_COORD_ID);

    m_vertexBuffer->bind();
    m_program->setAttributeBuffer(VERTEX_POS_ID, GL_FLOAT, 0, 3);

    m_textureBuffer->bind();
    m_program->setAttributeBuffer(TEXTURE_COORD_ID, GL_FLOAT, 0, 2);

    m_bg->bind();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    m_bg->release();

    m_light->bind();
    glDrawArrays(GL_TRIANGLE_STRIP, 4, 4);
    m_light->release();

    m_topText->bind();
    glDrawArrays(GL_TRIANGLE_STRIP, 8, 4);
    m_topText->release();

    m_program->disableAttributeArray(VERTEX_POS_ID);
    m_program->disableAttributeArray(TEXTURE_COORD_ID);

    glDepthMask(1);
    m_program->release();


    //Transparent
    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
}

static unsigned int loopTime = 0;
void OpenGLBackGround::fadeIn()
{
    opacityText = 0.0;
    opacityLight = 0.0;
    loopTime = 0;
    o_timer->start(10);
}

void OpenGLBackGround::fadeInStep()
{
    loopTime++;
    if(loopTime >= 50 && loopTime <= 120) {
        opacityLight = (loopTime - 50) / 70.0f;
    }
    if(loopTime >= 100 && loopTime <= 130) {
        opacityText = (loopTime - 100) / 30.0f;
    }
    if(loopTime > 130) {
        opacityText = 1.0f;
        opacityLight = 1.0f;
        o_timer->stop();
    }
}
