/**************************************************
 *
 * 機能： MainWindow
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "openglwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    OpenGLWidget *openGLWidget;

private slots:
    void setTitle(int newFps, int newTpf);

public slots:
    void open();
};

#endif // MAINWINDOW_H
