/**************************************************
 *
 * 機能： OpenGL画面
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#include "openglwidget.h"
#include <QApplication>

OpenGLWidget::OpenGLWidget(QWidget *parent)
    : QGLWidget(parent)
    , m_background(0)
    , m_mesh(0)
    , m_lighting(0)
    , m_phongShaderProgram(0)
    , MVP_ID(0)
    , MODEL_MATRIX_ID(0)
    , VIEW_MATRIX_ID(0)
    , m_bgShaderProgram(0)
    , m_proj(QMatrix4x4())
    , m_view(QMatrix4x4())
    , m_model(QMatrix4x4())
    , m_distExp(1830)
    , isLoaded(false)
    , m_animating(false)
    , m_trackBalls(0)
    , isTransing(false)
    , translateX(0.0)
    , translateY(0.0)
    , translateZ(-2.0)
    , lastPos(QVector2D())
    , transRatio(3.0f)
    , turnTimerInterval(0)
    , mModelScaleRatio_(1.2)
    , mModelPosition_(QVector3D())
    , mModelDirection_(QVector4D())
    , mCameraDistance_(0.3)
    , mViewAngle_(45.0)
    , mIsOnModel_(false)
    , mOutRotate_(true)
    , configuration(new Configuration())
{
    setMouseTracking(true);

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));

    m_time.start();
}

OpenGLWidget::~OpenGLWidget()
{
    if(0 != m_mesh)
        delete m_mesh;

    if(0 != m_lighting)
        delete m_lighting;

    if(0 != m_background)
        delete m_background;

    if (0 != m_phongShaderProgram) {
        m_phongShaderProgram->removeAllShaders();
        delete m_phongShaderProgram;
    }
    if (0 != m_bgShaderProgram) {
        m_bgShaderProgram->removeAllShaders();
        delete m_bgShaderProgram;
    }
    if (0 != m_trackBalls)
        delete m_trackBalls;
}

void OpenGLWidget::wheelEvent(QWheelEvent *event)
{
    translateZ += event->delta() / 100;
}

void OpenGLWidget::mousePressEvent(QMouseEvent *event)
{
    if (m_trackBalls && isOnModel(m_mesh, event->localPos(), m_mvp)) {
        mIsOnModel_ = true;
        if (event->buttons() & Qt::LeftButton) {
            mOutRotate_ = isOutSide(m_mesh, event->localPos(),
                                    m_view, m_model, m_mvp);
            m_trackBalls[1].push(pixelPosToViewPos(event->localPos()), m_trackBalls[0].rotation().conjugate());
        }
        if (event->buttons() & Qt::RightButton) {
            m_trackBalls[1].push(pixelPosToViewPos(event->localPos()), m_trackBalls[0].rotation().conjugate());
            isTransing = true;
        }
        event->setAccepted(true);
        lastPos = QVector2D(event->localPos());
    } else {
        mIsOnModel_ = false;
        event->setAccepted(false);
    }
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (m_trackBalls && mIsOnModel_) {
        if (event->buttons() & Qt::LeftButton) {
            m_trackBalls[1].move(pixelPosToViewPos(event->localPos()), m_trackBalls[0].rotation().conjugate(), mOutRotate_);
        } else {
            m_trackBalls[1].release(pixelPosToViewPos(event->localPos()), m_trackBalls[0].rotation().conjugate());
        }

        if ((event->buttons() & Qt::RightButton) && isTransing) {
            moveTo(event->localPos());
        }
        lastPos = QVector2D(event->localPos());
    }

    if(mIsOnModel_) {
        event->setAccepted(true);
    } else {
        event->setAccepted(false);
    }
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_trackBalls) {
        if (event->button() == Qt::LeftButton) {
            m_trackBalls[1].release(pixelPosToViewPos(event->localPos()), m_trackBalls[0].rotation().conjugate());
        }

        if (event->button() == Qt::RightButton && isTransing) {
            isTransing = false;
            m_trackBalls[1].release(pixelPosToViewPos(event->localPos()), m_trackBalls[0].rotation().conjugate());
        }

        if(mIsOnModel_) {
            event->setAccepted(true);
        } else {
            event->setAccepted(false);
        }
    }
    mIsOnModel_ = false;
}

void OpenGLWidget::hideEvent(QHideEvent *)
{
    setAnimating(false);
}

void OpenGLWidget::showEvent(QShowEvent *)
{
    initPara();
    setAnimating(true);

    if(m_background)
        m_background->fadeIn();
}

bool OpenGLWidget::importModel(QString pFile)
{
    if (m_mesh)
        delete m_mesh;

    m_mesh = new Mesh(m_phongShaderProgram);
    isLoaded = m_mesh->loadMesh(pFile);

    if(!isLoaded) {
        qDebug() << "Error: "<< aiGetErrorString();
        delete m_mesh;
    } else {
        m_mesh->setModelScale(mModelScaleRatio_);
        qDebug()<<"OpenGLWidget::importModel ok";
    }
    return isLoaded;
}

void OpenGLWidget::setAnimating(bool animating)
{
    m_animating = animating;
    if (m_animating)
        timer->start(turnTimerInterval);
    else
        timer->stop();
}

void OpenGLWidget::initializeGL()
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    prepareShaderProgram();
    initPara();

    isLoaded = importModel(configuration->getFilePath());


    if (isLoaded)
        setAnimating(true);
}

void OpenGLWidget::initPara()
{
    if (0 != configuration) {
        turnTimerInterval = configuration->getTurnTimerInterval();
        mCameraDistance_ = configuration->getCameraDistance();
        mViewAngle_ = configuration->getViewAngle();
        mModelScaleRatio_ = configuration->getScaleRatio();
        mModelPosition_ = configuration->getModelPosition();
        mModelDirection_ = configuration->getModelDirection();

        initLights(configuration->getLights());
        initModel();
        initBackGround();
    }
}

void OpenGLWidget::initModel()
{
    translateX = mModelPosition_.x();
    translateY = mModelPosition_.y();

    initTrackBalls();
}

void OpenGLWidget::paintGL()
{
    static int lastTime = m_time.elapsed();
    int currentTime = m_time.elapsed();
    if ((currentTime - lastTime) > 1000) {
        fps = m_frame * 1000 / (currentTime - lastTime);
        m_frame = 0;
        lastTime = currentTime;
        emit fpsChanged(fps, tpf);
    }
    m_frame++;

    if (isLoaded) {
        glClear(GL_COLOR_BUFFER_BIT |  GL_DEPTH_BUFFER_BIT);

        m_view.setToIdentity();
        m_view.lookAt(QVector3D(0, 0, mCameraDistance_), QVector3D(0, 0, 0), QVector3D(0, 1, 0));

        m_background->draw();

        m_model.setToIdentity();
        m_model.translate(translateX, translateY, translateZ);
        m_model.scale(m_mesh->getModelScaleRatio());

        m_model.rotate(m_trackBalls[1].rotation());
        m_model.translate(-m_mesh->center.x(), -m_mesh->center.y(), -m_mesh->center.z());

        m_phongShaderProgram->bind();
        m_mvp = m_proj * m_view * m_model;
        m_phongShaderProgram->setUniformValue(MVP_ID, m_mvp);
        m_phongShaderProgram->setUniformValue(MODEL_MATRIX_ID, m_model);
        m_phongShaderProgram->setUniformValue(VIEW_MATRIX_ID, m_view);

        m_lighting->calcViewPositon(m_view);
        m_lighting->bind();

        m_mesh->draw();

        m_phongShaderProgram->release();
    }

    tpf = m_time.elapsed() - currentTime;
}

void OpenGLWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);

    qreal aspect = qreal(width) / qreal(height ? height : 1);

    m_proj.setToIdentity();
    m_proj.perspective(mViewAngle_, aspect, 1.0, 1000.0);
}

QPointF OpenGLWidget::pixelPosToViewPos(const QPointF& p)
{
    return QPointF(2.0 * float(p.x()) / width() - 1.0,
                   1.0 - 2.0 * float(p.y()) / height());

}

void OpenGLWidget::prepareShaderProgram()
{
    m_phongShaderProgram = new QOpenGLShaderProgram(this);

    this->makeCurrent();
    if(!m_phongShaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/resources/shader/light.vert"))
        qCritical() << "can not load Vertex Shader: " << m_phongShaderProgram->log();

    if(!m_phongShaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/resources/shader/light.frag"))
        qCritical() << "can not load Fragment Shader: " << m_phongShaderProgram->log();

    if(!m_phongShaderProgram->link())
        qCritical() << "can not link Shader: " << m_phongShaderProgram->log();

    MVP_ID = m_phongShaderProgram->uniformLocation("MVP");
    MODEL_MATRIX_ID = m_phongShaderProgram->uniformLocation("M");
    VIEW_MATRIX_ID = m_phongShaderProgram->uniformLocation("V");

    m_bgShaderProgram = new QOpenGLShaderProgram(this);

    if(!m_bgShaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/resources/shader/background.vert"))
        qCritical() << "can not load Vertex Shader: " << m_bgShaderProgram->log();

    if(!m_bgShaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/resources/shader/background.frag"))
        qCritical() << "can not load Fragment Shader: " << m_bgShaderProgram->log();

    if(!m_bgShaderProgram->link())
        qCritical() << "can not link Shader: " << m_bgShaderProgram->log();
}

void OpenGLWidget::initLights(QVector<Light> lights)
{
    if (m_lighting)
        delete m_lighting;

    m_lighting = new Lighting(m_phongShaderProgram);

    for (int i = 0; i < MAX_LIGHTS; i++) {
        Lighting::LightIndex index = Lighting::Light0;
        switch(i){
        case 0:
            index = Lighting::Light0;
            break;
        case 1:
            index = Lighting::Light1;
            break;
        case 2:
            index = Lighting::Light2;
            break;
        case 3:
            index = Lighting::Light3;
            break;
        case 4:
            index = Lighting::Light4;
            break;
        case 5:
            index = Lighting::Light5;
            break;
        case 6:
            index = Lighting::Light6;
            break;
        case 7:
            index = Lighting::Light7;
            break;
        }

        m_lighting->glLightMode(index, lights[i].mode);
        m_lighting->glLight(index, Lighting::LightPower, lights[i].light_power);
        m_lighting->glLight(index, Lighting::LightAmbient, lights[i].ambient);
        m_lighting->glLight(index, Lighting::LightDiffuse, lights[i].diffuse);
        m_lighting->glLight(index, Lighting::LightSpecular, lights[i].specular);
        m_lighting->glLight(index, Lighting::LightPosition, lights[i].position);
        m_lighting->glLight(index, Lighting::LightSpotDirection, lights[i].spot_direction);
        m_lighting->glLight(index, Lighting::LightSpotExponent, lights[i].spot_exponent);
        m_lighting->glLight(index, Lighting::LightSpotCutoff, lights[i].spot_cutoff);
        m_lighting->glLight(index, Lighting::LightConstantAttenuation, lights[i].constant_attenuation);
        m_lighting->glLight(index, Lighting::LightLinearAttenuation, lights[i].linear_attenuation);
        m_lighting->glLight(index, Lighting::LightQuadraticAttenuation, lights[i].quadratic_attenuation);
        if (lights[i].enabled)
            m_lighting->glLightEnable(index);
    }
}

void OpenGLWidget::initTrackBalls()
{
    if (m_trackBalls)
        delete[] m_trackBalls;

    m_trackBalls = new TrackBall[2];
    m_trackBalls[0] = TrackBall(0.0f, QVector3D(0, 1, 0), TrackBall::Plane);
    m_trackBalls[1] = TrackBall(0.5f, QVector3D(0, 1, 0), createInitRotation(), TrackBall::Sphere);
}

void OpenGLWidget::initBackGround()
{
    if (m_background)
        delete m_background;

    m_background = new OpenGLBackGround(m_bgShaderProgram);
    m_background->loadBackGround(":/resources/image/background.jpg",
                                 ":/resources/image/n21_bg.png",
                                 ":/resources/image/n21_txt.png");
}


QQuaternion OpenGLWidget::createInitRotation()
{
    return QQuaternion::fromAxisAndAngle(mModelDirection_.x(),
                                         mModelDirection_.y(),
                                         mModelDirection_.z(),
                                         mModelDirection_.w());
}
void OpenGLWidget::moveTo(const QPointF &pos)
{
    translateX += (pos.x() - lastPos.x()) * transRatio / width();
    translateY -= (pos.y() - lastPos.y()) * transRatio / height();
}

bool OpenGLWidget::isOnModel(Mesh* mesh, const QPointF& point, const QMatrix4x4 mvp)
{
    bool bRet = false;
    if (0 != mesh) {
        QVector3D point_w = unProject(point, mvp);
        if(( mesh->min.x() < point_w.x()  ) &&
                ( mesh->min.y() < point_w.y()  ) &&
                ( mesh->min.z() < point_w.z()  ) &&
                ( point_w.x()   < mesh->max.x()) &&
                ( point_w.y()   < mesh->max.y()) &&
                ( point_w.z()   < mesh->max.z()))
            bRet = true;
    }
    return bRet;
}

bool OpenGLWidget::isOutSide(Mesh* mesh, const QPointF& point,
                             const QMatrix4x4 view,
                             const QMatrix4x4 model,
                             const QMatrix4x4 mvp)
{
    QVector4D center_c = view * model * QVector4D(mesh->center, 1.0f);
    QVector4D test_point_c = view * model * QVector4D(unProject(point, mvp), 1.0f);

    float center_length = center_c.length();
    float test_point_length = test_point_c.length();
    if (center_length > test_point_length)
        return true;
    else
        return false;
}

QVector3D OpenGLWidget::unProject(const QPointF& point, const QMatrix4x4 mvp)
{
    QVector3D worldCoordinates;
    float winX = point.x();
    float winY = height() - point.y();
    float winZ = 1.0;

    const QMatrix4x4 m = mvp.inverted();

    glReadPixels( winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

    QVector4D in;
    in.setX( winX / width()  * 2.0 - 1.0 );
    in.setY( winY / height() * 2.0 - 1.0 );
    in.setZ( 2.0 * winZ - 1.0 );
    in.setW( 1.0 );


    QVector4D out(m * in);
    if (out.w() == 0.0) {
        worldCoordinates = QVector3D(0.0, 0.0, 0.0);
        return worldCoordinates;
    }

    out.setW( 1.0 / out.w() );
    worldCoordinates.setX(out.x() * out.w());
    worldCoordinates.setY(out.y() * out.w());
    worldCoordinates.setZ(out.z() * out.w());
    return worldCoordinates;
}
