/**************************************************
 *
 * 機能： 照明システム
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-09
 * 更新時間： 2013-09-11
 *
 **************************************************/

#include "lighting.h"

Light::Light()
{
    // デフォルト値
    mode = Directional;
    enabled = false;
    light_power = 1;
    ambient = QVector4D(0.0, 0.0, 0.0, 1.0);
    diffuse = QVector4D(1.0, 1.0, 1.0, 1.0);
    specular = QVector4D(1.0, 1.0, 1.0, 1.0);
    position = QVector4D(0.0, 0.0, 1.0, 0.0);
    position_c = QVector4D(0.0, 0.0, 1.0, 0.0);
    spot_direction = QVector3D(0.0, 0.0, -1.0);
    spot_exponent = 0.0;
    spot_cutoff = 180.0;
    constant_attenuation = 1.0;
    linear_attenuation = 0.0;
    quadratic_attenuation = 0.0;
}

Light::~Light()
{
}

// 光源を有効にする
void Light::enable()
{
    enabled = true;
}

// 光源を無効にする
void Light::disable()
{
    enabled = false;
}

Lighting::Lighting(QOpenGLShaderProgram *program)
    : m_program(program)
{
    m_lights.resize(MAX_LIGHTS);
    for (unsigned int i = 0 ; i < MAX_LIGHTS ; i++) {
        m_lights.append(Light());

        // シェーダ内のに光源構造体へのハンドルIDを取得する
        LIGHTS_ID[i].mode = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].mode").toLatin1().data());
        LIGHTS_ID[i].enabled = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].enabled").toLatin1().data());
        LIGHTS_ID[i].light_power = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].light_power").toLatin1().data());
        LIGHTS_ID[i].ambient = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].ambient").toLatin1().data());
        LIGHTS_ID[i].diffuse = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].diffuse").toLatin1().data());
        LIGHTS_ID[i].specular = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].specular").toLatin1().data());
        LIGHTS_ID[i].position = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].position").toLatin1().data());
        LIGHTS_ID[i].position_c = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].position_c").toLatin1().data());
        LIGHTS_ID[i].spot_direction = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].spot_direction").toLatin1().data());
        LIGHTS_ID[i].spot_exponent = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].spot_exponent").toLatin1().data());
        LIGHTS_ID[i].spot_cutoff = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].spot_cutoff").toLatin1().data());
        LIGHTS_ID[i].constant_attenuation = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].constant_attenuation").toLatin1().data());
        LIGHTS_ID[i].linear_attenuation = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].linear_attenuation").toLatin1().data());
        LIGHTS_ID[i].quadratic_attenuation = m_program->uniformLocation(QString("gLights["+QString::number(i)+"].quadratic_attenuation").toLatin1().data());
    }
}

Lighting::~Lighting()
{
    m_program = NULL;
    m_lights.clear();
}

// 光源のパラメータを現在バインドしているシェーダに渡す
void Lighting::bind()
{
    for (int i = 0 ; i < MAX_LIGHTS ; i++) {
        m_program->setUniformValue(LIGHTS_ID[i].mode, m_lights[i].mode);
        m_program->setUniformValue(LIGHTS_ID[i].enabled, m_lights[i].enabled);
        m_program->setUniformValue(LIGHTS_ID[i].light_power, m_lights[i].light_power);
        m_program->setUniformValue(LIGHTS_ID[i].ambient, m_lights[i].ambient);
        m_program->setUniformValue(LIGHTS_ID[i].diffuse, m_lights[i].diffuse);
        m_program->setUniformValue(LIGHTS_ID[i].specular, m_lights[i].specular);
        m_program->setUniformValue(LIGHTS_ID[i].position, m_lights[i].position);
        m_program->setUniformValue(LIGHTS_ID[i].position_c, m_lights[i].position_c);
        m_program->setUniformValue(LIGHTS_ID[i].spot_direction, m_lights[i].spot_direction);
        m_program->setUniformValue(LIGHTS_ID[i].spot_exponent, m_lights[i].spot_exponent);
        m_program->setUniformValue(LIGHTS_ID[i].spot_cutoff, m_lights[i].spot_cutoff);
        m_program->setUniformValue(LIGHTS_ID[i].constant_attenuation, m_lights[i].constant_attenuation);
        m_program->setUniformValue(LIGHTS_ID[i].linear_attenuation, m_lights[i].linear_attenuation);
        m_program->setUniformValue(LIGHTS_ID[i].quadratic_attenuation, m_lights[i].quadratic_attenuation);
    }
}

// 光源のカメラ座標系の位置を計算する
void Lighting::calcViewPositon(QMatrix4x4 view)
{
    for (int i = 0 ; i < m_lights.size() ; i++) {
        m_lights[i].position_c = view * m_lights[i].position;
    }
}

// 指定されたインティクスの光源のタイプを設定する
void Lighting::glLightMode(LightIndex index, Light::LightMode mode)
{
    m_lights[index].mode = mode;
}

// 指定されたインティクスの光源の指定されたパラメータを設定する
void Lighting::glLight(LightIndex index, LightParam method, float value)
{
    if (method == LightPower)
        m_lights[index].light_power = value;
    else if (method == LightSpotExponent)
        m_lights[index].spot_exponent = value;
    else if (method == LightSpotCutoff)
        m_lights[index].spot_cutoff = value;
    else if (method == LightConstantAttenuation)
        m_lights[index].constant_attenuation = value;
    else if (method == LightLinearAttenuation)
        m_lights[index].linear_attenuation = value;
    else if (method == LightQuadraticAttenuation)
        m_lights[index].quadratic_attenuation = value;
}

// 指定されたインティクスの光源の指定されたパラメータを設定する
void Lighting::glLight(LightIndex index, LightParam method, QVector4D value)
{
    if (method == LightAmbient)
        m_lights[index].ambient = value;
    else if (method == LightDiffuse)
        m_lights[index].diffuse = value;
    else if (method == LightSpecular)
        m_lights[index].specular = value;
    else if (method == LightPosition)
        m_lights[index].position = value;
}

// 指定されたインティクスの光源の指定されたパラメータを設定する
void Lighting::glLight(LightIndex index, LightParam method, QVector3D value)
{
    if(method == LightSpotDirection) {
        m_lights[index].spot_direction = value;
    }
}

// 指定されたインティクスの光源を有効にする
void Lighting::glLightEnable(LightIndex index)
{
    m_lights[index].enable();
}

// 指定されたインティクスの光源を無効にする
void Lighting::glLightDisable(LightIndex index)
{
    m_lights[index].disable();
}
