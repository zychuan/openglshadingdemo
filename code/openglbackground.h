/**************************************************
 *
 * 機能： 背景の描画
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-10-06
 *
 **************************************************/

#ifndef OPENGLBACKGROUND_H
#define OPENGLBACKGROUND_H

#include <QObject>
#include <QTimer>
#include <QVector2D>
#include <QVector3D>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>

class OpenGLBackGround : public QObject
{
    Q_OBJECT

public:
    explicit OpenGLBackGround(QOpenGLShaderProgram *program);
    ~OpenGLBackGround();
    void loadBackGround(QString bg,
                        QString light,
                        QString topText);
    void draw();
    void fadeIn();

private slots:
    void fadeInStep();

private:
    QOpenGLShaderProgram *m_program;
    int S_MVP_ID;
    int S_OPACITY_TEXT_ID;
    int S_OPACITY_LIGHT_ID;

    QOpenGLBuffer *m_vertexBuffer;
    QOpenGLBuffer *m_textureBuffer;
    int VERTEX_POS_ID;
    int TEXTURE_COORD_ID;

    QOpenGLTexture *m_bg;
    QOpenGLTexture *m_topText;
    QOpenGLTexture *m_light;

    QMatrix4x4 s_mvp;
    float opacityText;
    float opacityLight;
    QTimer *o_timer;
};

#endif // OpenGLBackGround_H
