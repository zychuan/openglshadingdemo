/**************************************************
 *
 * 機能： 照明システム
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-09
 * 更新時間： 2013-09-11
 *
 **************************************************/

#ifndef LIGHTING_H
#define LIGHTING_H

#include <QVector>
#include <QVector4D>
#include <QOpenGLShaderProgram>

// 最大光源数
#define MAX_LIGHTS 8

// ライト構造体
struct Light {
    Light();
    ~Light();
    void enable();
    void disable();

    enum LightMode
    {
        Directional,            // 平行光
        Point,                  // 点光源
        Spot,                   // スポットライト
        Global,                 // グロバル環境光
    };
    LightMode mode;             // ライトタイプ
    bool enabled;               // 有効かどうか
    float light_power;          // 光の強さ
    QVector4D ambient;          // 環境光の色
    QVector4D diffuse;          // 拡散光の色
    QVector4D specular;         // 鏡面光の色
    QVector4D position;         // 光源の位置(ワールド座標系)
    QVector4D position_c;       // 光源の位置(カメラ座標系)
    QVector3D spot_direction;   // 照明の方向
    float spot_exponent;        // 輝度の分布
    float spot_cutoff;          // 最大放射角度
    float constant_attenuation; // 光の強さの定数減衰率
    float linear_attenuation;   // 距離による光の強さの線形減衰率
    float quadratic_attenuation;// 距離による光の強さの2次減衰率
};

class Lighting
{

public:
    Lighting(QOpenGLShaderProgram *program);
    ~Lighting();

    //　光源のインティクス
    enum LightIndex
    {
        Light0,
        Light1,
        Light2,
        Light3,
        Light4,
        Light5,
        Light6,
        Light7,
    };

    // 設定するための光源パラメータ
    enum LightParam
    {
        LightPower,               // 光の強さ
        LightAmbient,             // 環境光の色
        LightDiffuse,             // 拡散光の色
        LightSpecular,            // 鏡面光の色
        LightPosition,            // 光源の位置(ワールド座標系)
        LightSpotDirection,       // 照明の方向
        LightSpotExponent,        // 輝度の分布
        LightSpotCutoff,          // 最大放射角度
        LightConstantAttenuation, // 光の強さの定数減衰率
        LightLinearAttenuation,   // 距離による光の強さの線形減衰率
        LightQuadraticAttenuation,// 距離による光の強さの2次減衰率
    };

    /*
     * 機能： 光源のパラメータを現在バインドしているシェーダに渡す
     */
    void bind();

    /*
     * 機能： 光源のカメラ座標系の位置を計算する
     */
    void calcViewPositon(QMatrix4x4 view);

    /*
     * 機能： 指定されたインティクスの光源を有効にする
     */
    void glLightEnable(LightIndex index);

    /*
     * 機能： 指定されたインティクスの光源を無効にする
     */
    void glLightDisable(LightIndex index);

    /*
     * 機能： 指定されたインティクスの光源のタイプを設定する
     */
    void glLightMode(LightIndex index, Light::LightMode mode);

    /*
     * 機能： 指定されたインティクスの光源の指定されたパラメータを設定する
     */
    void glLight(LightIndex index, LightParam method, float value);

    /*
     * 機能： 指定されたインティクスの光源の指定されたパラメータを設定する
     */
    void glLight(LightIndex index, LightParam method, QVector4D value);

    /*
     * 機能： 指定されたインティクスの光源の指定されたパラメータを設定する
     */
    void glLight(LightIndex index, LightParam method, QVector3D value);

private:
    // 描画用のシェーダプログラム
    QOpenGLShaderProgram *m_program;

    // すべての光源を保存する
    QVector<Light> m_lights;

    // シェーダ内のに光源構造体へのハンドルID
    struct {
        int mode;
        int enabled;
        int light_power;
        int ambient;
        int diffuse;
        int specular;
        int position;
        int position_c;
        int spot_direction;
        int spot_exponent;
        int spot_cutoff;
        int constant_attenuation;
        int linear_attenuation;
        int quadratic_attenuation;
    } LIGHTS_ID[MAX_LIGHTS];
};

#endif // LIGHTING_H
