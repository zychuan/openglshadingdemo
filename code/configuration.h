/**************************************************
 *
 * 機能： 3Dモデルと照明要素読み込むクラス
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "lighting.h"
#include <QString>
#include <QVector3D>
#include <QVector4D>
#include <QtXml>

class Configuration
{
public:
    Configuration();
    bool getInitFlag() const;
    QString getFilePath() const;
    float getScaleRatio() const;
    float getViewAngle() const;
    float getCameraDistance() const;
    QVector3D getModelPosition() const;
    QVector4D getModelDirection() const;
    float getTurnTimerInterval() const;
    QVector<Light> getLights() const;

private:
    bool initFlag;
    QString filePath;
    float scaleRatio;
    float viewAngle;
    float cameraDistance;
    QVector3D modelPosition;
    QVector4D modelDirection;
    float turnTimerInterval;
    QVector<Light> mLights;
};

#endif // CONFIGURATION_H
