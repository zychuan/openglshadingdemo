/**************************************************
 *
 * 機能： 3Dモデルと照明要素読み込むクラス
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#include "configuration.h"
#include <QCoreApplication>

namespace {
    const QString OPENGL_MODEL_PATH = "modelPath";
    const QString OPENGL_TURN_TIMER_INTERVAL = "turnTimerInterval";
    const QString OPENGL_CAMERA_DISTANCE = "cameraDistance";
    const QString OPENGL_MODEL_DIRECTION = "modelDirection";
    const QString OPENGL_MODEL_POSITION = "modelPosition";
    const QString OPENGL_MODEL_SCALE_RATIO = "scaleRatio";
    const QString OPENGL_VIEW_ANGLE = "viewAngle";
    const QString OPENGL_MODEL_DIRECTION_ANGLE = "angle";
    const QString OPENGL_LIGHT = "light";
    const QString OPENGL_LIGHT_ID = "id";
    const QString OPENGL_LIGHT_ENABLE = "enable";
    const QString OPENGL_LIGHT_MODE = "mode";
    const QString OPENGL_LIGHT_POWER = "power";
    const QString OPENGL_LIGHT_AMBIENT = "ambient";
    const QString OPENGL_LIGHT_DIFFUSE = "diffuse";
    const QString OPENGL_LIGHT_SPECULAR = "specular";
    const QString OPENGL_LIGHT_POSITION = "position";
    const QString OPENGL_LIGHT_SPOT_DIRECTION = "spotDirection";
    const QString OPENGL_LIGHT_SPOT_EXPONENT = "spotExponent";
    const QString OPENGL_LIGHT_SPOT_CUTOFF = "spotCutoff";
    const QString OPENGL_LIGHT_CONSTANT_ATTENUATION = "constantAttenuation";
    const QString OPENGL_LIGHT_LINEAR_ATTENUATION = "linearAttenuation";
    const QString OPENGL_LIGHT_QUADRATIC_ATTENUATION = "quadraticAttenuation";
    const QString OPENGL_LIGHT_X = "x";
    const QString OPENGL_LIGHT_Y = "y";
    const QString OPENGL_LIGHT_Z = "z";
    const QString OPENGL_LIGHT_W = "w";
    const QString OPENGL_LIGHT_RED = "red";
    const QString OPENGL_LIGHT_GREEN = "green";
    const QString OPENGL_LIGHT_BLUE = "blue";
    const QString OPENGL_LIGHT_ALPHA = "alpha";
}

Configuration::Configuration()
    : initFlag(true)
    , filePath("")
    , scaleRatio(1.2)
    , viewAngle(45.0)
    , cameraDistance(3.0f)
    , modelPosition(QVector3D())
    , modelDirection(QVector4D())
    , turnTimerInterval(16)
    , mLights(QVector<Light>())
{
    const QString path = "openglconf.xml";
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly))
    {
        initFlag = false;
        return;
    }

    QDomDocument doc;

    if(!doc.setContent(&file))
    {
        file.close();
        initFlag = false;
        return;
    }
    file.close();
    QDomElement root = doc.documentElement();

    QDomNode modelPathNode = root.elementsByTagName(OPENGL_MODEL_PATH).at(0);
    if(!modelPathNode.isNull())
        filePath = modelPathNode.toElement().text();

    QDomNode scaleRatioNode = root.elementsByTagName(OPENGL_MODEL_SCALE_RATIO).at(0);
    if(!scaleRatioNode.isNull())
        scaleRatio = scaleRatioNode.toElement().text().toFloat();

    QDomNode viewAngleNode = root.elementsByTagName(OPENGL_VIEW_ANGLE).at(0);
    if(!viewAngleNode.isNull())
        viewAngle = viewAngleNode.toElement().text().toFloat();

    QDomNode cameraDistanceNode = root.elementsByTagName(OPENGL_CAMERA_DISTANCE).at(0);
    if(!cameraDistanceNode.isNull())
        cameraDistance = cameraDistanceNode.toElement().text().toFloat();

    QDomNode turnTimerIntervalNode = root.elementsByTagName(OPENGL_TURN_TIMER_INTERVAL).at(0);
    if(!turnTimerIntervalNode.isNull())
        turnTimerInterval = turnTimerIntervalNode.toElement().text().toFloat();

    QDomNode modelPositionNode = root.elementsByTagName(OPENGL_MODEL_POSITION).at(0);
    if(!modelPositionNode.isNull()) {
        QDomAttr xAttr = modelPositionNode.toElement().attributeNode(OPENGL_LIGHT_X);
        QDomAttr yAttr = modelPositionNode.toElement().attributeNode(OPENGL_LIGHT_Y);
        QDomAttr zAttr = modelPositionNode.toElement().attributeNode(OPENGL_LIGHT_Z);
        if(!xAttr.isNull())
            modelPosition.setX(xAttr.value().toFloat());
        if(!yAttr.isNull())
            modelPosition.setY(yAttr.value().toFloat());
        if(!zAttr.isNull())
            modelPosition.setZ(zAttr.value().toFloat());
    }

    QDomNode modelDirectionNode = root.elementsByTagName(OPENGL_MODEL_DIRECTION).at(0);
    if(!modelDirectionNode.isNull()) {
        QDomAttr angleAttr = modelDirectionNode.toElement().attributeNode(OPENGL_MODEL_DIRECTION_ANGLE);
        QDomAttr xAttr = modelDirectionNode.toElement().attributeNode(OPENGL_LIGHT_X);
        QDomAttr yAttr = modelDirectionNode.toElement().attributeNode(OPENGL_LIGHT_Y);
        QDomAttr zAttr = modelDirectionNode.toElement().attributeNode(OPENGL_LIGHT_Z);
        if(!angleAttr.isNull())
            modelDirection.setW(angleAttr.value().toFloat());
        if(!xAttr.isNull())
            modelDirection.setX(xAttr.value().toFloat());
        if(!yAttr.isNull())
            modelDirection.setY(yAttr.value().toFloat());
        if(!zAttr.isNull())
            modelDirection.setZ(zAttr.value().toFloat());
    }

    QDomNodeList lightNodeList = root.elementsByTagName(OPENGL_LIGHT);

    mLights.clear();
    mLights.resize(MAX_LIGHTS);
    for (unsigned int i = 0 ; i < MAX_LIGHTS ; i++)
        mLights.append(Light());
    if(!lightNodeList.isEmpty()) {
        for(int i = 0; i < lightNodeList.length(); i++) {
            QDomNode lightNode = lightNodeList.item(i);
            QDomElement lightElement = lightNode.toElement();

            int lightId = lightElement.attribute(OPENGL_LIGHT_ID).toInt();


            int enable = lightElement.attribute(OPENGL_LIGHT_ENABLE).toInt();
            if (enable)
                mLights[lightId].enabled = true;

            int mode = lightElement.attribute(OPENGL_LIGHT_MODE).toInt();
            switch(mode){
            case 0:
                mLights[lightId].mode = Light::Directional;
                break;
            case 1:
                mLights[lightId].mode = Light::Point;
                break;
            case 2:
                mLights[lightId].mode = Light::Spot;
                break;
            case 3:
                mLights[lightId].mode = Light::Global;
                break;
            default:
                mLights[lightId].mode = Light::Directional;
                break;
            }

            QDomNode powerNode = lightNode.namedItem(OPENGL_LIGHT_POWER);
            QDomNode ambientNode = lightNode.namedItem(OPENGL_LIGHT_AMBIENT);
            QDomNode diffuseNode = lightNode.namedItem(OPENGL_LIGHT_DIFFUSE);
            QDomNode specularNode = lightNode.namedItem(OPENGL_LIGHT_SPECULAR);
            QDomNode positionNode = lightNode.namedItem(OPENGL_LIGHT_POSITION);
            QDomNode spotDirNode = lightNode.namedItem(OPENGL_LIGHT_SPOT_DIRECTION);
            QDomNode spotExpNode = lightNode.namedItem(OPENGL_LIGHT_SPOT_EXPONENT);
            QDomNode spotCutNode = lightNode.namedItem(OPENGL_LIGHT_SPOT_CUTOFF);
            QDomNode cAttenNode = lightNode.namedItem(OPENGL_LIGHT_CONSTANT_ATTENUATION);
            QDomNode lAttenNode = lightNode.namedItem(OPENGL_LIGHT_LINEAR_ATTENUATION);
            QDomNode qAttenNode = lightNode.namedItem(OPENGL_LIGHT_QUADRATIC_ATTENUATION);

            if (!powerNode.isNull())
                mLights[lightId].light_power = powerNode.toElement().text().toFloat();

            if (!ambientNode.isNull()) {
                QDomElement ambientElement = ambientNode.toElement();
                float red = ambientElement.attribute(OPENGL_LIGHT_RED).toFloat();
                float green = ambientElement.attribute(OPENGL_LIGHT_GREEN).toFloat();
                float blue = ambientElement.attribute(OPENGL_LIGHT_BLUE).toFloat();
                float alpha = ambientElement.attribute(OPENGL_LIGHT_ALPHA).toFloat();
                mLights[lightId].ambient = QVector4D(red, green, blue, alpha);
            }

            if (!diffuseNode.isNull()) {
                QDomElement diffuseElement = diffuseNode.toElement();
                float red = diffuseElement.attribute(OPENGL_LIGHT_RED).toFloat();
                float green = diffuseElement.attribute(OPENGL_LIGHT_GREEN).toFloat();
                float blue = diffuseElement.attribute(OPENGL_LIGHT_BLUE).toFloat();
                float alpha = diffuseElement.attribute(OPENGL_LIGHT_ALPHA).toFloat();
                mLights[lightId].diffuse = QVector4D(red, green, blue, alpha);
            }

            if (!specularNode.isNull()) {
                QDomElement specularElement = specularNode.toElement();
                float red = specularElement.attribute(OPENGL_LIGHT_RED).toFloat();
                float green = specularElement.attribute(OPENGL_LIGHT_GREEN).toFloat();
                float blue = specularElement.attribute(OPENGL_LIGHT_BLUE).toFloat();
                float alpha = specularElement.attribute(OPENGL_LIGHT_ALPHA).toFloat();
                mLights[lightId].specular = QVector4D(red, green, blue, alpha);
            }

            if (!positionNode.isNull()) {
                QDomElement positionElement = positionNode.toElement();
                float x = positionElement.attribute(OPENGL_LIGHT_X).toFloat();
                float y = positionElement.attribute(OPENGL_LIGHT_Y).toFloat();
                float z = positionElement.attribute(OPENGL_LIGHT_Z).toFloat();
                float w = positionElement.attribute(OPENGL_LIGHT_W).toFloat();
                mLights[lightId].position = QVector4D(x, y, z, w);
            }

            if (!spotDirNode.isNull()) {
                QDomElement spotDirElement = spotDirNode.toElement();
                float x = spotDirElement.attribute(OPENGL_LIGHT_X).toFloat();
                float y = spotDirElement.attribute(OPENGL_LIGHT_Y).toFloat();
                float z = spotDirElement.attribute(OPENGL_LIGHT_Z).toFloat();
                mLights[lightId].spot_direction = QVector3D(x, y, z);
            }

            if (!spotExpNode.isNull())
                mLights[lightId].spot_exponent = spotExpNode.toElement().text().toFloat();

            if (!spotCutNode.isNull())
                mLights[lightId].spot_cutoff = spotCutNode.toElement().text().toFloat();

            if (!cAttenNode.isNull())
                mLights[lightId].constant_attenuation = cAttenNode.toElement().text().toFloat();

            if (!lAttenNode.isNull())
                mLights[lightId].linear_attenuation = lAttenNode.toElement().text().toFloat();

            if (!qAttenNode.isNull())
                mLights[lightId].quadratic_attenuation = qAttenNode.toElement().text().toFloat();
        }
    }
}

QString Configuration::getFilePath() const
{
    return filePath;
}

float Configuration::getScaleRatio() const
{
    return scaleRatio;
}

float Configuration::getViewAngle() const
{
    return viewAngle;
}

float Configuration::getCameraDistance() const
{
    return cameraDistance;
}

QVector3D Configuration::getModelPosition() const
{
    return modelPosition;
}
QVector4D Configuration::getModelDirection() const
{
    return modelDirection;
}

float Configuration::getTurnTimerInterval() const
{
    return turnTimerInterval;
}

QVector<Light> Configuration::getLights() const
{
    return mLights;
}
