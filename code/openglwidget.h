/**************************************************
 *
 * 機能： OpenGL画面
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/
#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include "trackball.h"
#include "mesh.h"
#include "openglbackground.h"
#include "lighting.h"

#include "configuration.h"
#include <QTimer>
#include <QGLWidget>
#include <QVector>
#include <QVector3D>
#include <QVector4D>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QResizeEvent>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

class OpenGLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit OpenGLWidget(QWidget *parent = 0);
    ~OpenGLWidget();

    bool importModel(QString pFile);

    unsigned int fps;
    unsigned int tpf;

    void setAnimating(bool animating);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL( int width, int height );

    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void hideEvent(QHideEvent *);
    void showEvent(QShowEvent *);

signals:
    void fpsChanged(int newFps, int newTpf);


private:
    void prepareShaderProgram();
    void initPara();
    void initLights(QVector<Light> lights);
    void initTrackBalls();
    void initBackGround();

    void initModel();

    QQuaternion createInitRotation();
    void moveTo(const QPointF &pos);
    QPointF pixelPosToViewPos(const QPointF& p);
    bool isOnModel(Mesh* mesh, const QPointF& point,
                   const QMatrix4x4 mvp);
    bool isOutSide(Mesh* mesh, const QPointF& point,
                   const QMatrix4x4 view,
                   const QMatrix4x4 model,
                   const QMatrix4x4 mvp);
    QVector3D unProject(const QPointF& point, const QMatrix4x4 mvp);

    OpenGLBackGround                *m_background;
    Mesh                            *m_mesh;

    Lighting                        *m_lighting;

    QTime                           m_time;
    int                             m_frame;

    QOpenGLShaderProgram            *m_phongShaderProgram;
    int                             MVP_ID;
    int                             MODEL_MATRIX_ID;
    int                             VIEW_MATRIX_ID;

    QOpenGLShaderProgram            *m_bgShaderProgram;

    QMatrix4x4                      m_mvp;
    QMatrix4x4                      m_proj;
    QMatrix4x4                      m_view;
    QMatrix4x4                      m_model;

    int                             m_distExp;
    bool                            isLoaded;

    bool                            m_animating;

    QTimer                          *timer;
    TrackBall                       *m_trackBalls;

    // 平行移動座標
    bool 							isTransing;
    float 							translateX;
    float                           translateY;
    float                           translateZ;

    // マウスの位置
    QVector2D 						lastPos;
    float 							transRatio;

    float                           turnTimerInterval;
    float               			mModelScaleRatio_;
    QVector3D               		mModelPosition_;
    QVector4D               		mModelDirection_;
    float               			mCameraDistance_;
    float                           mViewAngle_;
    bool                            mIsOnModel_;
    bool                            mOutRotate_;

    Configuration                   *configuration;
};

#endif // OPENGLWIDGET_H
