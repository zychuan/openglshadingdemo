#ifndef TRACKBALL_H
#define TRACKBALL_H

#include <QTime>
#include <QPointF>
#include <QVector3D>
#include <QQuaternion>

#define PI 3.14159265358979

class TrackBall
{
public:
    enum TrackMode
    {
        Plane,
        Sphere,
    };
    TrackBall(TrackMode mode = Sphere);
    TrackBall(float angularVelocity, const QVector3D& axis, TrackMode mode = Sphere);
    TrackBall(float angularVelocity, const QVector3D& axis, const QQuaternion& rotation, TrackMode mode = Sphere);

    void push(const QPointF& p, const QQuaternion &transformation);
    void move(const QPointF& p, const QQuaternion &transformation, bool outRotate = true);
    void release(const QPointF& p, const QQuaternion &transformation);

    void start(); // starts clock
    void stop(); // stops clock
    QQuaternion rotation() const;
private:

    QQuaternion m_rotation;
    QVector3D m_axis;
    float m_angularVelocity;

    QPointF m_lastPos;
    QTime m_lastTime;
    bool m_paused;
    bool m_pressed;
    TrackMode m_mode;
};

#endif // TRACKBALL_H
