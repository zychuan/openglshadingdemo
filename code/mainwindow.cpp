/**************************************************
 *
 * 機能： MainWindow
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    openGLWidget = new OpenGLWidget();
    setCentralWidget(openGLWidget);
    openGLWidget->setAnimating(true);

    // FPSをウインドウタイトルに更新する
    connect(openGLWidget, SIGNAL(fpsChanged(int, int)), SLOT(setTitle(int, int)));
}

MainWindow::~MainWindow()
{
    delete openGLWidget;
    delete ui;
}

// FPSをウインドウタイトルに更新する
void MainWindow::setTitle(int newFps, int newTpf)
{
    //FPSをウインドウタイトルに表示する
    QString title = windowTitle();
    QStringList tArr = title.split("-");
    if (tArr.size() >= 3)
    {
        tArr[1] = QString("FPS:" + QString::number(newFps));
        tArr[2] = QString("TPF:" + QString::number(newTpf));
        setWindowTitle(tArr.join("-"));
    }
}

//ダイアログウィンドウからモデルファイルを選択しロードする
void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open model"), "", tr("Model Files (*.3ds *.obj *.dae *.fbx)"));

    if (fileName != "")
    {
        bool isLoaded = openGLWidget->importModel(fileName);
        if(isLoaded)
        {
            QString title = windowTitle();
            title = title.split("-")[0];
            setWindowTitle(title + "-" + QString("FPS:" + QString::number(openGLWidget->fps)) + "-" + QString("TPF:" + QString::number(openGLWidget->tpf)) + "-" + fileName);
        }
        else
        {
            QMessageBox::information(NULL, "Error", "Can not load file: " + fileName, QMessageBox::Ok);
        }
    }
}
