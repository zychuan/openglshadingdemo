/**************************************************
 *
 * 機能： モデルをロード、描画する
 * 作成者： Zhang-Yunchuan
 * 更新者： Zhang-Yunchuan
 * 作成時間： 2013-09-03
 * 更新時間： 2013-09-11
 *
 **************************************************/

#include "mesh.h"


Mesh::MeshEntry::MeshEntry()
{
    m_vertexBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_indexBuffer = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    m_normalBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_textureBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

    indicesNum  = 0;

    // 材質のデフォルト値
    m_material.diffuse = QVector4D(0.8f, 0.8f, 0.8f, 1.0f);
    m_material.ambient = QVector4D(0.2f, 0.2f, 0.2f, 1.0f);
    m_material.specular = QVector4D(0.0f, 0.0f, 0.0f, 1.0f);
    m_material.emission = QVector4D(0.0f, 0.0f, 0.0f, 1.0f);
    m_material.shininess = 0.0;
    m_material.texCount = 0;
}

Mesh::MeshEntry::~MeshEntry()
{
    if (m_vertexBuffer->isCreated()) {
        m_vertexBuffer->destroy();
    }

    if (m_normalBuffer->isCreated()) {
        m_normalBuffer->destroy();
    }

    if (m_indexBuffer->isCreated()) {
        m_indexBuffer->destroy();
    }

    if (m_textureBuffer->isCreated()) {
        m_textureBuffer->destroy();
    }
}

Mesh::Mesh(QOpenGLShaderProgram *program)
    : scene(0)
    , m_program(program)
    , magnification(1.0)
    , modelLength(1.0)
    , scaleRatio(1.0)
{
    // 各シェーダ内の変数へのハンドルIDを取得する
    VERTEX_POS_ID = m_program->attributeLocation("vertexPos_m");
    VERTEX_NORM_ID = m_program->attributeLocation("vertexNorm_m");
    TEXTURE_COORD_ID = m_program->attributeLocation("texture_coord");
    NODE_TRANS_MATRIX_ID = m_program->uniformLocation("NodeTrafo");
    MATERIAL_ID.diffuse = m_program->uniformLocation("gMaterial.diffuse");
    MATERIAL_ID.ambient = m_program->uniformLocation("gMaterial.ambient");
    MATERIAL_ID.specular = m_program->uniformLocation("gMaterial.specular");
    MATERIAL_ID.emission = m_program->uniformLocation("gMaterial.emission");
    MATERIAL_ID.shininess = m_program->uniformLocation("gMaterial.shininess");
    MATERIAL_ID.texCount = m_program->uniformLocation("gMaterial.texCount");
}


Mesh::~Mesh()
{
    clear();
    aiReleaseImport(scene);
    m_program = NULL;
}

// ロードしたテクスチャをクリアする
void Mesh::clear()
{
    QMapIterator<QString, QOpenGLTexture*> i(m_textures);
    while(i.hasNext())
    {
        i.next();
        i.value()->destroy();
    }
    m_textures.clear();
    m_entries.clear();
}

// 拡大縮小倍率を設定する
void Mesh::setModelScale(float ratio)
{
    magnification = ratio;

    // モデル拡大縮小倍率を計算する
    scaleRatio = magnification / modelLength;
}

// 計算したモデル拡大縮小倍率を取得する
float Mesh::getModelScaleRatio()
{
    return scaleRatio;
}

//モデルの各部分のバウンディングボックスの最大最少座標値を求める
void Mesh::getBoundingBoxForNode (aiNode* nd,
                                  aiVector3D* min,
                                  aiVector3D* max,
                                  aiMatrix4x4* trafo
                                  )
{
    // ノードの変換行列を求める
    aiMatrix4x4 prev = *trafo;
    aiMultiplyMatrix4(trafo, &nd->mTransformation);

    // ノードの各メッシュをトラバーサルし、最大最少値を求める
    for (unsigned int n = 0; n < nd->mNumMeshes; ++n) {
        const struct aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];
        for (unsigned int t = 0; t < mesh->mNumVertices; ++t) {
            aiVector3D tmp = mesh->mVertices[t];
            aiTransformVecByMatrix4(&tmp, trafo);

            min->x = qMin(min->x, tmp.x);
            min->y = qMin(min->y, tmp.y);
            min->z = qMin(min->z, tmp.z);

            max->x = qMax(max->x, tmp.x);
            max->y = qMax(max->y, tmp.y);
            max->z = qMax(max->z, tmp.z);
        }
    }

    // ノードの子ノードを求める
    for (unsigned int i = 0; i < nd->mNumChildren; ++i)
        getBoundingBoxForNode(nd->mChildren[i], min, max, trafo);

    *trafo = prev;
}

// モデルのバウンディングボックスの最大最少座標値を求める
void Mesh::getBoundingBox (aiVector3D* min, aiVector3D* max)
{
    aiMatrix4x4 trafo;
    aiIdentityMatrix4(&trafo);

    min->x = min->y = min->z =  1e10f;
    max->x = max->y = max->z = -1e10f;
    getBoundingBoxForNode(scene->mRootNode, min, max, &trafo);
}

// aiColor4D型をfloat配列に変換する
void Mesh::color4ToFloat4(aiColor4D *c, float f[4])
{
    f[0] = c->r; f[1] = c->g; f[2] = c->b; f[3] = c->a;
}

// float配列に値を設定する
void Mesh::setFloat4(float f[4], float a, float b, float c, float d)
{
    f[0] = a; f[1] = b; f[2] = c; f[3] = d;
}

// 指定だれたファイルパスのモデルファイルをロードし、解析する
bool Mesh::loadMesh(QString filename)
{
    clear();

    // モデルファイルからロードする
    scene = aiImportFile(filename.toLatin1().data(), aiProcessPreset_TargetRealtime_Fast);

    // フォルダパスを求める
    QDir baseDir = QFileInfo(filename).absoluteDir();
    if (scene) {
        // 頂点法線などの情報を取得し、メッシュオブジェクトを作成する。
        initFromScene(scene, baseDir);

        // 中心点の座標を求める
        aiVector3D sceneMin, sceneMax, sceneCenter;
        getBoundingBox(&sceneMin, &sceneMax);
        sceneCenter.x = (sceneMin.x + sceneMax.x) / 2.0f;
        sceneCenter.y = (sceneMin.y + sceneMax.y) / 2.0f;
        sceneCenter.z = (sceneMin.z + sceneMax.z) / 2.0f;

        // モデルの拡大縮小倍率を計算する
        modelLength = sceneMax.x - sceneMin.x;
        modelLength = qMax(sceneMax.y - sceneMin.y, modelLength);
        modelLength = qMax(sceneMax.z - sceneMin.z, modelLength);
        scaleRatio = magnification / modelLength;

        min = QVector3D(sceneMin.x, sceneMin.y, sceneMin.z);
        max = QVector3D(sceneMax.x, sceneMax.y, sceneMax.z);
        center = QVector3D(sceneCenter.x, sceneCenter.y, sceneCenter.z);

        return true;
    } else { // sceneが存在しないと、ロード失敗
        return false;
    }
}

// Assimpがロードしたモデルから、頂点法線などの情報を取得し、メッシュオブジェクトを作成する
bool Mesh::initFromScene(const aiScene* pScene, QDir baseDir)
{
    m_entries.resize(pScene->mNumMeshes);

    for (unsigned int n = 0; n < pScene->mNumMeshes; ++n)
    {
        const aiMesh* mesh = pScene->mMeshes[n];

        // 頂点インデックス配列を作成
        unsigned int *indexArray = new unsigned int[mesh->mNumFaces * 3];
        unsigned int faceIndex = 0;
        for (unsigned int t = 0; t < mesh->mNumFaces; ++t) {
            const aiFace* face = &mesh->mFaces[t];
            memcpy(&indexArray[faceIndex], face->mIndices, 3 * sizeof(unsigned int));
            faceIndex += 3;
        }

        // 頂点インデックスバッファを作成し、バインドする
        m_entries[n].m_indexBuffer->create();
        m_entries[n].m_indexBuffer->setUsagePattern(QOpenGLBuffer::StaticDraw);
        m_entries[n].m_indexBuffer->bind();
        m_entries[n].m_indexBuffer->allocate(indexArray, sizeof(unsigned int) * mesh->mNumFaces * 3);

        m_entries[n].indicesNum = mesh->mNumFaces * 3;

        // 頂点バッファを作成し、バインドする
        m_entries[n].m_vertexBuffer->create();
        m_entries[n].m_vertexBuffer->setUsagePattern(QOpenGLBuffer::StaticDraw);
        m_entries[n].m_vertexBuffer->bind();
        m_entries[n].m_vertexBuffer->allocate(mesh->mVertices, sizeof(float)*3*mesh->mNumVertices);

        // 法線バッファを作成し、バインドする
        m_entries[n].m_normalBuffer->create();
        m_entries[n].m_normalBuffer->setUsagePattern(QOpenGLBuffer::StaticDraw);
        m_entries[n].m_normalBuffer->bind();
        m_entries[n].m_normalBuffer->allocate(mesh->mNormals, sizeof(float)*3*mesh->mNumVertices);

        // テクスチャバッファを作成し、バインドする
        float *texCoords = new float[mesh->mNumVertices * 2];
        if (mesh->HasTextureCoords(0)) {
            for (unsigned int k = 0; k < mesh->mNumVertices; ++k) {
                texCoords[k*2]   = mesh->mTextureCoords[0][k].x;
                texCoords[k*2+1] = mesh->mTextureCoords[0][k].y;
            }
        } else {
            for (unsigned int k = 0; k < mesh->mNumVertices; ++k) {
                texCoords[k*2]   = 0.0;
                texCoords[k*2+1] = 0.0;
            }
        }

        m_entries[n].m_textureBuffer->create();
        m_entries[n].m_textureBuffer->setUsagePattern(QOpenGLBuffer::StaticDraw);
        m_entries[n].m_textureBuffer->bind();
        m_entries[n].m_textureBuffer->allocate(texCoords, sizeof(float)*2*mesh->mNumVertices);


        // メッシュの材質を取得する
        aiMaterial *mtl = scene->mMaterials[mesh->mMaterialIndex];

        // テクスチャがある場合、テクスチャオブジェクトを作成し、テクスチャファイルのパスをkeyとしてm_texturesに保存する
        aiString texPath;
        if(AI_SUCCESS == mtl->GetTexture(aiTextureType_DIFFUSE, 0, &texPath)){
            // テクスチャオブジェクトがすでにある場合、作成しない
            QString fileName(texPath.data);
            if(!m_textures.contains(fileName)) {
                QImage tmpImage(baseDir.filePath(fileName));

                QOpenGLTexture *texture = new QOpenGLTexture(tmpImage.mirrored());
                texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
                texture->setMagnificationFilter(QOpenGLTexture::Linear);

                m_textures.insert(fileName, texture);
            }

            m_entries[n].textureName = fileName;
            m_entries[n].m_material.texCount = 1;
        }
        else
            m_entries[n].m_material.texCount = 0;

        // 拡散光材質を取得する
        float c[4];
        setFloat4(c, 0.8f, 0.8f, 0.8f, 1.0f);
        aiColor4D diffuse;
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
            color4ToFloat4(&diffuse, c);
        m_entries[n].m_material.diffuse = QVector4D(c[0], c[1], c[2], c[3]);

        // 環境光材質を取得する
        setFloat4(c, 0.2f, 0.2f, 0.2f, 1.0f);
        aiColor4D ambient;
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &ambient))
            color4ToFloat4(&ambient, c);
        m_entries[n].m_material.ambient = QVector4D(c[0], c[1], c[2], c[3]);

        // 鏡面光材質を取得する
        setFloat4(c, 0.0f, 0.0f, 0.0f, 1.0f);
        aiColor4D specular;
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &specular))
            color4ToFloat4(&specular, c);
        m_entries[n].m_material.specular = QVector4D(c[0], c[1], c[2], c[3]);

        // 放射輝度を取得する
        setFloat4(c, 0.0f, 0.0f, 0.0f, 1.0f);
        aiColor4D emission;
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission))
            color4ToFloat4(&emission, c);
        m_entries[n].m_material.emission = QVector4D(c[0], c[1], c[2], c[3]);

        // 鏡面光の指数を取得する
        float shininess = 0.0;
        unsigned int max = 1;
        aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
        m_entries[n].m_material.shininess = shininess;
    }

    return true;
}

// モデルを描画する
void Mesh::draw()
{
    aiMatrix4x4 trafo;
    aiIdentityMatrix4(&trafo);

    // ルートノードから各ノードを再帰的に描画する
    renderNode(scene->mRootNode, &trafo);
}

// 指定されたノードを描画する
void Mesh::renderNode(aiNode* nd, aiMatrix4x4* trafo)
{
    // ノードの変換行列を求める
    aiMatrix4x4 prev = *trafo;
    aiMultiplyMatrix4(trafo, &nd->mTransformation);

    // ノードの各メッシュを描画する
    m_program->enableAttributeArray(VERTEX_POS_ID);
    m_program->enableAttributeArray(VERTEX_NORM_ID);
    m_program->enableAttributeArray(TEXTURE_COORD_ID);
    for (unsigned int n = 0 ; n < nd->mNumMeshes; ++n) {
        // メッシュのパラメータを現在バインドしているシェーダに渡す
        m_program->setUniformValue(NODE_TRANS_MATRIX_ID, QMatrix4x4((float*)trafo));
        m_program->setUniformValue(MATERIAL_ID.diffuse, m_entries[nd->mMeshes[n]].m_material.diffuse);
        m_program->setUniformValue(MATERIAL_ID.ambient, m_entries[nd->mMeshes[n]].m_material.ambient);
        m_program->setUniformValue(MATERIAL_ID.specular, m_entries[nd->mMeshes[n]].m_material.specular);
        m_program->setUniformValue(MATERIAL_ID.emission, m_entries[nd->mMeshes[n]].m_material.emission);
        m_program->setUniformValue(MATERIAL_ID.shininess, m_entries[nd->mMeshes[n]].m_material.shininess);
        m_program->setUniformValue(MATERIAL_ID.texCount, m_entries[nd->mMeshes[n]].m_material.texCount);


        m_entries[nd->mMeshes[n]].m_vertexBuffer->bind();
        m_program->setAttributeBuffer(VERTEX_POS_ID, GL_FLOAT, 0, 3);

        m_entries[nd->mMeshes[n]].m_normalBuffer->bind();
        m_program->setAttributeBuffer(VERTEX_NORM_ID, GL_FLOAT, 0, 3);

        m_entries[nd->mMeshes[n]].m_textureBuffer->bind();
        m_program->setAttributeBuffer(TEXTURE_COORD_ID, GL_FLOAT, 0, 2);

        // テクスチャがある場合バインドする
        if (m_entries[nd->mMeshes[n]].m_material.texCount > 0) {
            m_textures[m_entries[nd->mMeshes[n]].textureName]->bind();
        }

        m_entries[nd->mMeshes[n]].m_indexBuffer->bind();
        // 描画
        glDrawElements(GL_TRIANGLES, m_entries[nd->mMeshes[n]].indicesNum, GL_UNSIGNED_INT, 0);

        if (m_entries[nd->mMeshes[n]].m_material.texCount > 0) {
            m_textures[m_entries[nd->mMeshes[n]].textureName]->release();
        }

    }
    m_program->disableAttributeArray(VERTEX_POS_ID);
    m_program->disableAttributeArray(VERTEX_NORM_ID);
    m_program->disableAttributeArray(TEXTURE_COORD_ID);

    // ノードの子ノードを描画する
    for (unsigned int i = 0; i < nd->mNumChildren; ++i)
        renderNode(nd->mChildren[i], trafo);

    *trafo = prev;
}
