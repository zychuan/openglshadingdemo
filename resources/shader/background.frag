varying vec2 texCoord;
varying float posZ;

uniform sampler2D gSampler;
uniform float opacityText;
uniform float opacityLight;
void main()
{
    float op = 1.0;
    if(posZ == 0.3)
        op = opacityLight;
    else if(posZ == 0.4)
        op = opacityText;

    gl_FragColor = texture2D(gSampler, texCoord) * op;
}
