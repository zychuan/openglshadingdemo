attribute vec3 vertexPos_m;
attribute vec2 texture_coord;

uniform mat4 MVP;

varying vec2 texCoord;
varying float posZ;

void main()
{
    posZ = vertexPos_m.z;
    gl_Position = MVP * vec4(vertexPos_m, 1.0);
    texCoord = texture_coord;
}
