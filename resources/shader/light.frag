const int MAX_LIGHTS = 8;
const int LIGHT_MODE_DIRECTIONAL = 0;
const int LIGHT_MODE_POINT = 1;
const int LIGHT_MODE_SPOT = 2;
const int LIGHT_MODE_GLOBAL = 3;

struct Material
{
    vec4 diffuse;
    vec4 ambient;
    vec4 specular;
    vec4 emission;
    float shininess;
    int texCount;
};

struct Light
{
    int mode;
    bool enabled;
    float light_power;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position;
    vec4 position_c;
    vec3 spot_direction;
    float spot_exponent;
    float spot_cutoff;
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;
};

varying vec3 vertexPos_c;
varying vec3 vertexNorm_c;
varying vec2 tex_coord;

uniform Material gMaterial;
uniform Light gLights[MAX_LIGHTS];
uniform sampler2D gSampler;

vec4 calcLightInternal(Light light, vec3 lightDir)
{
    vec4 AmbientColor = gMaterial.ambient * light.ambient;
    vec4 DiffuseColor  = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 SpecularColor = vec4(0.0, 0.0, 0.0, 0.0);

    vec3 n = normalize(vertexNorm_c);
    vec3 l = normalize(lightDir);
    float DiffuseFactor = dot(n, l);
    if(DiffuseFactor > 0.0) {
        DiffuseColor = gMaterial.diffuse * light.diffuse * light.light_power * DiffuseFactor;

        vec3 E = normalize(-vertexPos_c);
        vec3 R = normalize(reflect(-l, n));
        float SpecularFactor = dot(E, R);
        if (SpecularFactor > 0.0) {
            if (gMaterial.shininess > 0.0) {
                SpecularFactor = pow(SpecularFactor, gMaterial.shininess);
                SpecularColor = gMaterial.specular * light.specular * light.light_power * SpecularFactor;
            }
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 calcPointLight(Light light)
{
    vec3 LightDir_c = light.position_c.xyz - vertexPos_c;

    float distance = length(light.position_c.xyz - vertexPos_c);
    LightDir_c = normalize(LightDir_c);

    vec4 Color = calcLightInternal(light, LightDir_c);
    float attenuation = light.constant_attenuation +
            light.linear_attenuation * distance +
            light.quadratic_attenuation * distance * distance;

    return Color / attenuation;
}

vec4 calcDirectionalLight(Light light)
{
    return calcLightInternal(light, light.position_c.xyz);
}

vec4 calcSpotLight(Light light)
{
    vec3 LightDir_c = light.position_c.xyz - vertexPos_c;
    float distance = length(LightDir_c);
    LightDir_c = normalize(LightDir_c);

    float angle = acos(dot(-LightDir_c, light.spot_direction));
    float cutoff = radians(clamp(light.spot_cutoff, 0.0, 90.0));
    vec4 AmbientColor = gMaterial.ambient * light.ambient;
    float attenuation = light.constant_attenuation +
            light.linear_attenuation * distance +
            light.quadratic_attenuation * distance * distance;
    if (angle < cutoff) {
        float spotFactor = 0.0;
        if (light.spot_exponent == 0.0)
            spotFactor = 1;
        else
            spotFactor = pow(dot(-LightDir_c, light.spot_direction), light.spot_exponent);
        vec4 Color = calcLightInternal(light, LightDir_c);
        return spotFactor * Color / attenuation;
    } else
        return vec4(0,0,0,0);
}

vec4 calcGlobalAmbientLight(Light light)
{
    return light.ambient * gMaterial.ambient;
}

void main()
{
    vec4 totalLight = gMaterial.emission;

    for (int i = 0 ; i < MAX_LIGHTS ; i++) {
        if (gLights[i].enabled) {
            if(gLights[i].mode == LIGHT_MODE_DIRECTIONAL)
                totalLight += calcDirectionalLight(gLights[i]);
            else if(gLights[i].mode == LIGHT_MODE_POINT)
                totalLight += calcPointLight(gLights[i]);
            else if(gLights[i].mode == LIGHT_MODE_SPOT)
                totalLight += calcSpotLight(gLights[i]);
            else if(gLights[i].mode == LIGHT_MODE_GLOBAL)
                totalLight += calcGlobalAmbientLight(gLights[i]);
        }
    }

    if (gMaterial.texCount > 0) {
        gl_FragColor = texture2D(gSampler, tex_coord) * totalLight;
    }
    else {
        gl_FragColor = totalLight;
    }
}
