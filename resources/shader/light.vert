attribute vec3 vertexPos_m;
attribute vec3 vertexNorm_m;
attribute vec2 texture_coord;

varying vec3 vertexPos_c;
varying vec3 vertexNorm_c;
varying vec2 tex_coord;

uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform mat4 NodeTrafo;

void main()
{
    mat4 model = M * NodeTrafo;
    gl_Position = MVP * NodeTrafo * vec4(vertexPos_m, 1.0);
    tex_coord = texture_coord;
    vertexPos_c = (V * model * vec4(vertexPos_m, 1)).xyz;
    vertexNorm_c = normalize((V * model * vec4(vertexNorm_m, 0.0)).xyz);
}
